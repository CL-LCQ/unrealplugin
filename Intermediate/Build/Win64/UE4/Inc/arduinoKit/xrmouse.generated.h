// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARDUINOKIT_xrmouse_generated_h
#error "xrmouse.generated.h already included, missing '#pragma once' in xrmouse.h"
#endif
#define ARDUINOKIT_xrmouse_generated_h

#define arduinoKit_Source_arduinoKit_xrmouse_h_17_RPC_WRAPPERS
#define arduinoKit_Source_arduinoKit_xrmouse_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define arduinoKit_Source_arduinoKit_xrmouse_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAxrmouse(); \
	friend struct Z_Construct_UClass_Axrmouse_Statics; \
public: \
	DECLARE_CLASS(Axrmouse, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/arduinoKit"), NO_API) \
	DECLARE_SERIALIZER(Axrmouse)


#define arduinoKit_Source_arduinoKit_xrmouse_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAxrmouse(); \
	friend struct Z_Construct_UClass_Axrmouse_Statics; \
public: \
	DECLARE_CLASS(Axrmouse, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/arduinoKit"), NO_API) \
	DECLARE_SERIALIZER(Axrmouse)


#define arduinoKit_Source_arduinoKit_xrmouse_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Axrmouse(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Axrmouse) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Axrmouse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Axrmouse); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Axrmouse(Axrmouse&&); \
	NO_API Axrmouse(const Axrmouse&); \
public:


#define arduinoKit_Source_arduinoKit_xrmouse_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Axrmouse(Axrmouse&&); \
	NO_API Axrmouse(const Axrmouse&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Axrmouse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Axrmouse); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Axrmouse)


#define arduinoKit_Source_arduinoKit_xrmouse_h_17_PRIVATE_PROPERTY_OFFSET
#define arduinoKit_Source_arduinoKit_xrmouse_h_14_PROLOG
#define arduinoKit_Source_arduinoKit_xrmouse_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_PRIVATE_PROPERTY_OFFSET \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_RPC_WRAPPERS \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_INCLASS \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define arduinoKit_Source_arduinoKit_xrmouse_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_PRIVATE_PROPERTY_OFFSET \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_INCLASS_NO_PURE_DECLS \
	arduinoKit_Source_arduinoKit_xrmouse_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID arduinoKit_Source_arduinoKit_xrmouse_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
