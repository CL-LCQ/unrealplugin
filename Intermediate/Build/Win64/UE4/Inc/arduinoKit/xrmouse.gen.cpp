// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "arduinoKit/xrmouse.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodexrmouse() {}
// Cross Module References
	ARDUINOKIT_API UClass* Z_Construct_UClass_Axrmouse_NoRegister();
	ARDUINOKIT_API UClass* Z_Construct_UClass_Axrmouse();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_arduinoKit();
// End Cross Module References
	void Axrmouse::StaticRegisterNativesAxrmouse()
	{
	}
	UClass* Z_Construct_UClass_Axrmouse_NoRegister()
	{
		return Axrmouse::StaticClass();
	}
	struct Z_Construct_UClass_Axrmouse_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Axrmouse_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_arduinoKit,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Axrmouse_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "xrmouse.h" },
		{ "ModuleRelativePath", "xrmouse.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Axrmouse_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Axrmouse>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Axrmouse_Statics::ClassParams = {
		&Axrmouse::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_Axrmouse_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_Axrmouse_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Axrmouse()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Axrmouse_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Axrmouse, 2665714397);
	static FCompiledInDefer Z_CompiledInDefer_UClass_Axrmouse(Z_Construct_UClass_Axrmouse, &Axrmouse::StaticClass, TEXT("/Script/arduinoKit"), TEXT("Axrmouse"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Axrmouse);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
