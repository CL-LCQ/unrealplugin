// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARDUINOKIT_arduinoKitGameModeBase_generated_h
#error "arduinoKitGameModeBase.generated.h already included, missing '#pragma once' in arduinoKitGameModeBase.h"
#endif
#define ARDUINOKIT_arduinoKitGameModeBase_generated_h

#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_RPC_WRAPPERS
#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAarduinoKitGameModeBase(); \
	friend struct Z_Construct_UClass_AarduinoKitGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AarduinoKitGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/arduinoKit"), NO_API) \
	DECLARE_SERIALIZER(AarduinoKitGameModeBase)


#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAarduinoKitGameModeBase(); \
	friend struct Z_Construct_UClass_AarduinoKitGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AarduinoKitGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/arduinoKit"), NO_API) \
	DECLARE_SERIALIZER(AarduinoKitGameModeBase)


#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AarduinoKitGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AarduinoKitGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AarduinoKitGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AarduinoKitGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AarduinoKitGameModeBase(AarduinoKitGameModeBase&&); \
	NO_API AarduinoKitGameModeBase(const AarduinoKitGameModeBase&); \
public:


#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AarduinoKitGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AarduinoKitGameModeBase(AarduinoKitGameModeBase&&); \
	NO_API AarduinoKitGameModeBase(const AarduinoKitGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AarduinoKitGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AarduinoKitGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AarduinoKitGameModeBase)


#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_12_PROLOG
#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_RPC_WRAPPERS \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_INCLASS \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID arduinoKit_Source_arduinoKit_arduinoKitGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
