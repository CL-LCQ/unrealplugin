//Based off the "Arduino and C++ (for Windows)" code found at: http://playground.arduino.cc/Interfacing/CPPWindows

#include "Serial.h"
#include "UnrealString.h"

#define BOOL2bool(B) B == 0 ? false : true
bool wasPressed = false;
bool isPressed = false;
int pushTracker = 0;
float theScale = 1.0f;

USerial* USerial::OpenComPort(bool &bOpened, int32 Port, int32 BaudRate)
{
	USerial* Serial = NewObject<USerial>();
	bOpened = Serial->Open(Port, BaudRate);
	return Serial;
}



int32 USerial::BytesToInt(TArray<uint8> Bytes)
{
	if (Bytes.Num() != 4)
	{
		return 0;
	}

	return *reinterpret_cast<int32*>(Bytes.GetData());
}




FVector USerial::getVectorFromLine(FString arduinoData, bool &bSuccess)
{

	bSuccess = false;

	FVector result = FVector(0.0f, 0.0f, 0.0f);

	int n = arduinoData.Len();
	
	
	//if there is data
	if (n > 0) {

		UE_LOG(LogTemp, Log, TEXT("incoming data: %s"), *arduinoData);
		//convert string to byte array 
		const char* arduinoBytes;
		FTCHARToUTF8 Converted(*arduinoData);
		arduinoBytes = Converted.Get();




		//size of the byte array from the string
		int size = sizeof(arduinoBytes);
		UE_LOG(LogTemp, Log, TEXT("size of array: %d"), size);

		//create the end of data integer
		uint8 endOfdata = 0x12;
		endOfdata = arduinoBytes[size - 2];

		//print the array
		for (int i = 0; i < size-1; i++) {
			UE_LOG(LogTemp, Error, TEXT("Byte = %d. "), arduinoBytes[i]);
		}



		//create a recipient for conversion
		char* correctedBytes = new char[20];

		//convert the bytes
		for (int i = 0; i < size ; i++)
		{
			correctedBytes[i + 2] = arduinoBytes[i];

		}


		//bitwise operations to reconstruct the data
		short x = (short)(correctedBytes[2] << 8 | correctedBytes[3]);
		short y = (short)(correctedBytes[4] << 8 | correctedBytes[5]);
		short z = (short)(correctedBytes[6] << 8 | correctedBytes[7]);

		//UE_LOG(LogTemp, Error, TEXT("X = %d. Y = %d. Z = %d."), x, y, z);

		//convert to floats
		float xF = (float)x / 100.0f;
		float yF = (float)y / 100.0f;
		float zF = (float)z / 100.0f;

		UE_LOG(LogTemp, Error, TEXT("X = %d. Y = %d. Z = %d."), xF, yF, zF);

		result = FVector(xF, yF, zF);

		UE_LOG(LogTemp, Error, TEXT("corrected byte 8 is: %d."), correctedBytes[8]);

		if (correctedBytes[8] == 127) {
			bSuccess = true;
		}

	}

	return result;
}


FVector USerial::getVectorFromBytes(TArray<uint8> Bytes, bool & bSuccess, float scale)
{

	FVector result = FVector(0.1f, 0.1f, 0.1f);

	//UE_LOG(LogTemp, Error, TEXT("byte num is: %d."), Bytes.Num());

	int size = Bytes.Num()-2;
	bSuccess = false;

	if (size > 0)
	{
		for (int i = 0; i < size; i++) {

			UE_LOG(LogTemp, Log, TEXT("byte %d. is: %d."), i, Bytes[i]);

		}


		//create a recipient for conversion
		//char* correctedBytes = new char[20];  //works
		TArray<uint8> correctedBytes;
		correctedBytes.Init(0, 20);

		//convert the bytes
		for (int i = 0; i < size; i++)
		{
			correctedBytes[i + 2] = Bytes[i];

		}

		//bitwise operations to reconstruct the data
		short x = (short)(correctedBytes[2] << 8 | correctedBytes[3]);
		short y = (short)(correctedBytes[4] << 8 | correctedBytes[5]);
		short z = (short)(correctedBytes[6] << 8 | correctedBytes[7]);

		//UE_LOG(LogTemp, Error, TEXT("X = %d. Y = %d. Z = %d."), x, y, z);

		//convert to floats
		float xF = (float)x/10.0f;
		float yF = (float)y/10.0f;
		float zF = (float)z/10.0f;



		UE_LOG(LogTemp, Log, TEXT("X = %d. Y = %d. Z = %d."), xF, yF, zF);

		//result = FVector(xF, yF, zF);
		result = FVector(yF, xF, zF) * scale;

		//UE_LOG(LogTemp, Log, TEXT("corrected byte 8 is: %d."), correctedBytes[8]);

		if (correctedBytes[8] == 127) {

			bSuccess = true;
			UE_LOG(LogTemp, Error, TEXT("success"));
		}
		else{
			UE_LOG(LogTemp, Error, TEXT("wrong data"));
			bSuccess = false;
		}


	}

	else{

		UE_LOG(LogTemp, Error, TEXT("wrong data"));
		bSuccess = false;
	}



	return result;
}

void USerial::getTransformFromBytes(TArray<uint8> Bytes, bool & bSuccess, float scale, FVector & OutPosition, FVector & OutOrientation)
{
	//add world offset for initial positoion

	//FQuat q;
	//OutPosition = FVector(0.1f, 0.1f, 0.1f);
	//OutOrientation = FVector(0.0f, 0.0f, 0.0f);

	int size = Bytes.Num() - 2;
	bSuccess = false;

	//UE_LOG(LogTemp, Log, TEXT("Data size is %d. "), size);


	if (size >= 26)
	{
		//print the data
		//for (int i = 0; i < size; i++) {

			//UE_LOG(LogTemp, Log, TEXT("byte %d. is: %d."), i, Bytes[i]);

		//}


		//create a recipient for conversion
		TArray<uint8> positionBytes;
		positionBytes.Init(0, 20);
		TArray<uint8> cobraBytes;
		cobraBytes.Init(0, 30);

		//pass the bytes to the arrays
		for (int i = 0; i < size; i++)
		{
			if (i < 7) {

				positionBytes[i + 2] = Bytes[i];
			}
			else {
				cobraBytes[i - 7] = Bytes[i];
			}

		}


		//check the data
		if (positionBytes[8] == 127) {

			bSuccess = true;
			

			//bitwise operations to reconstruct the data
			short x = (short)(positionBytes[2] << 8 | positionBytes[3]);
			short y = (short)(positionBytes[4] << 8 | positionBytes[5]);
			short z = (short)(positionBytes[6] << 8 | positionBytes[7]);

			//UE_LOG(LogTemp, Error, TEXT("X = %d. Y = %d. Z = %d."), x, y, z);

			//convert to floats
			float xF = (float)x / 10.0f;
			float yF = (float)y / 10.0f;
			float zF = (float)z / 10.0f;



			//UE_LOG(LogTemp, Log, TEXT("X = %d. Y = %d. Z = %d."), xF, -yF, zF);


			OutPosition  = FVector(yF, xF, zF*1.4) * theScale;

		}
		else {

			bSuccess = false;
		
		}

		if (cobraBytes[20] == 127) {

			

			if (cobraBytes[15] == 16) {
				//started to press
				isPressed = true;

			}
			if (isPressed == true && cobraBytes[15] != 16) {

				//pressed finished
				wasPressed = true;
				isPressed = false;
				UE_LOG(LogTemp, Log, TEXT("IN"));
			}

			if (wasPressed == true) {
				wasPressed = false;

				pushTracker += 1;
				UE_LOG(LogTemp, Log, TEXT("pressTracker = %d. "), pushTracker);

				if (pushTracker == 0) {
					theScale = 0.3;
				}
				else if (pushTracker == 1) {
					theScale = 2;

				}
				else if (pushTracker == 2) {
					theScale = 20;

				}
				else if (pushTracker == 3) {
					theScale = 400;

				}
				else {
					pushTracker = 0;
					theScale = 0.1;
				}

			}

			float Yaw, Pitch, Roll;

			TArray<uint8> gyroBuff;
			gyroBuff.Init(0, 3);

			gyroBuff[0] = cobraBytes[2];
			gyroBuff[1] = cobraBytes[3];
			gyroBuff[2] = cobraBytes[4];

			Yaw = gyroBuff[0] << 16 | gyroBuff[1] << 8 | gyroBuff[2];
			Yaw = Yaw / 10000.0f;

			if (Yaw > 1000)
			{
				Yaw -= 1317.0f;
			}



			gyroBuff[0] = cobraBytes[5];
			gyroBuff[1] = cobraBytes[6];
			gyroBuff[2] = cobraBytes[7];

			Pitch = gyroBuff[0] << 16 | gyroBuff[1] << 8 | gyroBuff[2];
			Pitch = Pitch / 10000.0f;

			if (Pitch > 1000)
			{
				Pitch -= 1317.0f;
			}



			gyroBuff[0] = cobraBytes[8];
			gyroBuff[1] = cobraBytes[9];
			gyroBuff[2] = cobraBytes[10];

			Roll = gyroBuff[0] << 16 | gyroBuff[1] << 8 | gyroBuff[2];
			Roll = Roll / 10000.0f;

			if (Roll > 1000)
			{
				Roll -= 1317.0f;
			}

			//orientation.y = Yaw;
			//orientation.x = -Pitch;
			//orientation.z = -Roll;

			
			// Abbreviations for the various angular functions
			

			OutOrientation = FVector(Roll, Pitch, Yaw); //this is the correct order for unreal

			//UE_LOG(LogTemp, Log, TEXT("X = %d. Y = %d. Z = %d."), Roll, Pitch, Yaw);
			bSuccess = true;
		}
		else {
			bSuccess = false;
		}

		if (bSuccess == true) {
			UE_LOG(LogTemp, Log, TEXT("success"));
		}
		else {
			UE_LOG(LogTemp, Error, TEXT("ERROR"));
		}

		
	}

	



}











TArray<uint8> USerial::IntToBytes(const int32 &Int)
{
	TArray<uint8> Bytes;
	Bytes.Append(reinterpret_cast<const uint8*>(&Int), 4);
	return Bytes;
}

float USerial::BytesToFloat(TArray<uint8> Bytes)
{
	if (Bytes.Num() != 4)
	{
		return 0;
	}

	return *reinterpret_cast<float*>(Bytes.GetData());
}

TArray<uint8> USerial::FloatToBytes(const float &Float)
{
	TArray<uint8> Bytes;
	Bytes.Append(reinterpret_cast<const uint8*>(&Float), 4);
	return Bytes;
}

USerial::USerial()
	: m_hIDComDev(NULL)
	, m_Port(-1)
	, m_Baud(-1)
	, WriteLineEnd(ELineEnd::n)
{
	FMemory::Memset(&m_OverlappedRead, 0, sizeof(OVERLAPPED));
	FMemory::Memset(&m_OverlappedWrite, 0, sizeof(OVERLAPPED));
}

USerial::~USerial()
{
	Close();
}

bool USerial::Open(int32 nPort, int32 nBaud)
{
	if (nPort < 0)
	{
		UE_LOG(LogTemp, Error, TEXT("Invalid port number: %d"), nPort);
		return false;
	}
	if (m_hIDComDev)
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to use opened Serial instance to open a new one. "
				"Current open instance port: %d | Port tried: %d"), m_Port, nPort);
		return false;
	}

	FString szPort;
	if (nPort < 10)
		szPort = FString::Printf(TEXT("COM%d"), nPort);
	else
		szPort = FString::Printf(TEXT("\\\\.\\COM%d"), nPort);
	DCB dcb;

	m_hIDComDev = CreateFile(*szPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
	if (m_hIDComDev == NULL)
	{
		unsigned long dwError = GetLastError();
		UE_LOG(LogTemp, Error, TEXT("Failed to open port COM%d (%s). Error: %08X"), nPort, *szPort, dwError);
		return false;
	}

	FMemory::Memset(&m_OverlappedRead, 0, sizeof(OVERLAPPED));
	FMemory::Memset(&m_OverlappedWrite, 0, sizeof(OVERLAPPED));

	COMMTIMEOUTS CommTimeOuts;
	CommTimeOuts.ReadIntervalTimeout = 6;
	//CommTimeOuts.ReadIntervalTimeout = 0xFFFFFFFF;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 0;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 10;
	SetCommTimeouts(m_hIDComDev, &CommTimeOuts);

	m_OverlappedRead.hEvent = CreateEvent(NULL, true, false, NULL);
	m_OverlappedWrite.hEvent = CreateEvent(NULL, true, false, NULL);

	dcb.DCBlength = sizeof(DCB);
	GetCommState(m_hIDComDev, &dcb);
	dcb.BaudRate = nBaud;
	dcb.ByteSize = 8;

	if (!SetCommState(m_hIDComDev, &dcb) ||
		!SetupComm(m_hIDComDev, 10000, 10000) ||
		m_OverlappedRead.hEvent == NULL ||
		m_OverlappedWrite.hEvent == NULL)
	{
		unsigned long dwError = GetLastError();
		if (m_OverlappedRead.hEvent != NULL) CloseHandle(m_OverlappedRead.hEvent);
		if (m_OverlappedWrite.hEvent != NULL) CloseHandle(m_OverlappedWrite.hEvent);
		CloseHandle(m_hIDComDev);
		m_hIDComDev = NULL;
		UE_LOG(LogTemp, Error, TEXT("Failed to setup port COM%d. Error: %08X"), nPort, dwError);
		return false;
	}

	//FPlatformProcess::Sleep(0.05f);
	AddToRoot();
	m_Port = nPort;
	m_Baud = nBaud;
	return true;
}

void USerial::Close()
{
	if (!m_hIDComDev) return;

	if (m_OverlappedRead.hEvent != NULL) CloseHandle(m_OverlappedRead.hEvent);
	if (m_OverlappedWrite.hEvent != NULL) CloseHandle(m_OverlappedWrite.hEvent);
	CloseHandle(m_hIDComDev);
	m_hIDComDev = NULL;

	RemoveFromRoot();
}

FString USerial::ReadString(bool &bSuccess)
{
	return ReadStringUntil(bSuccess, '\0');
}

FString USerial::Readln(bool &bSuccess)
{
	return ReadStringUntil(bSuccess, '\n');
}

FString USerial::ReadStringUntil(bool &bSuccess, uint8 Terminator)
{
	bSuccess = false;
	if (!m_hIDComDev) return TEXT("");

	TArray<uint8> Chars;
	uint8 Byte = 0x0;
	bool bReadStatus;
	unsigned long dwBytesRead, dwErrorFlags;
	COMSTAT ComStat;

	ClearCommError(m_hIDComDev, &dwErrorFlags, &ComStat);
	if (!ComStat.cbInQue) return TEXT("");

	do {
		bReadStatus = BOOL2bool(ReadFile(
			m_hIDComDev,
			&Byte,
			1,
			&dwBytesRead,
			&m_OverlappedRead));

		if (!bReadStatus)
		{
			if (GetLastError() == ERROR_IO_PENDING)
			{
				
				WaitForSingleObject(m_OverlappedRead.hEvent, 2000);
				
			}
			else
			{
				Chars.Add(0x0);
				break;
			}
		}

		if (Byte == Terminator || dwBytesRead == 0)
		{
			// when Terminator is \n, we know we're expecting lines from Arduino. But those
			// are ended in \r\n. That means that if we found the line Terminator (\n), our previous
			// character could be \r. If it is, we remove that from the array.
			if (Chars.Num() > 0 && Terminator == '\n' && Chars.Top() == '\r') Chars.Pop(false);

			Chars.Add(0x0);
			break;
		}
		else Chars.Add(Byte);

	} while (Byte != 0x0 && Byte != Terminator);

	bSuccess = true;
	auto Convert = FUTF8ToTCHAR((ANSICHAR*)Chars.GetData());
	return FString(Convert.Get());
}

float USerial::ReadFloat(bool &bSuccess)
{
	bSuccess = false;

	TArray<uint8> Bytes = ReadBytes(4);
	if (Bytes.Num() == 0) return 0;

	bSuccess = true;
	return *(reinterpret_cast<float*>(Bytes.GetData()));
}

int32 USerial::ReadInt(bool &bSuccess)
{
	bSuccess = false;

	TArray<uint8> Bytes = ReadBytes(4);
	if (Bytes.Num() == 0) return 0;

	bSuccess = true;
	return *(reinterpret_cast<int32*>(Bytes.GetData()));
}

uint8 USerial::ReadByte(bool &bSuccess)
{
	bSuccess = false;
	if (!m_hIDComDev) return 0x0;

	uint8 Byte = 0x0;
	bool bReadStatus;
	unsigned long dwBytesRead, dwErrorFlags;
	COMSTAT ComStat;

	ClearCommError(m_hIDComDev, &dwErrorFlags, &ComStat);
	if (!ComStat.cbInQue) return 0x0;

	bReadStatus = BOOL2bool(ReadFile(
		m_hIDComDev,
		&Byte,
		1,
		&dwBytesRead,
		&m_OverlappedRead));

	if (!bReadStatus)
	{
		if (GetLastError() == ERROR_IO_PENDING)
		{
			WaitForSingleObject(m_OverlappedRead.hEvent, 2000);
		}
		else
		{
			return 0x0;
		}
	}

	bSuccess = dwBytesRead > 0;
	return Byte;
}

TArray<uint8> USerial::ReadBytes(int32 Limit)
{
	TArray<uint8> Data;
	
	//if no hidcomdev end
	if (!m_hIDComDev) return Data;

	//empty the byffer
	Data.Empty(Limit);

	//create a buffer with the limits
	uint8* Buffer = new uint8[Limit];
	bool bReadStatus;
	unsigned long dwBytesRead, dwErrorFlags;
	COMSTAT ComStat;

	
	ClearCommError(m_hIDComDev, &dwErrorFlags, &ComStat);
	
	if (!ComStat.cbInQue) return Data;

	bReadStatus = BOOL2bool(ReadFile(
		m_hIDComDev,
		Buffer,
		Limit,
		&dwBytesRead,
		&m_OverlappedRead));

	if (!bReadStatus)
	{
		if (GetLastError() == ERROR_IO_PENDING)
		{
			
				WaitForSingleObject(m_OverlappedRead.hEvent, INFINITE);
			//WaitForSingleObject(m_OverlappedRead.hEvent, 2000);
		}
		else
		{
			return Data;
		}
	}

	//Data.Append(Buffer, dwBytesRead);
	Data.Append(Buffer, Limit);
	
	return Data;
}

bool USerial::Print(FString String)
{
	auto Convert = FTCHARToUTF8(*String);
	TArray<uint8> Data;
	Data.Append((uint8*)Convert.Get(), Convert.Length());

	return WriteBytes(Data);
}

bool USerial::Println(FString String)
{
	return Print(String + LineEndToStr(WriteLineEnd));
}

bool USerial::WriteFloat(float Value)
{
	TArray<uint8> Buffer;
	Buffer.Append(reinterpret_cast<uint8*>(&Value), 4);
	return WriteBytes(Buffer);
}

bool USerial::WriteInt(int32 Value)
{
	TArray<uint8> Buffer;
	Buffer.Append(reinterpret_cast<uint8*>(&Value), 4);
	return WriteBytes(Buffer);
}

bool USerial::WriteByte(uint8 Value)
{
	TArray<uint8> Buffer({ Value });
	return WriteBytes(Buffer);
}

bool USerial::WriteBytes(TArray<uint8> Buffer)
{
	if (!m_hIDComDev) false;

	bool bWriteStat;
	unsigned long dwBytesWritten;

	bWriteStat = BOOL2bool(WriteFile(m_hIDComDev, Buffer.GetData(), Buffer.Num(), &dwBytesWritten, &m_OverlappedWrite));
	if (!bWriteStat && (GetLastError() == ERROR_IO_PENDING))
	{
		if (WaitForSingleObject(m_OverlappedWrite.hEvent, 2000))
		{
			dwBytesWritten = 0;
			return false;
		}
		else
		{
			GetOverlappedResult(m_hIDComDev, &m_OverlappedWrite, &dwBytesWritten, false);
			m_OverlappedWrite.Offset += dwBytesWritten;
			return true;
		}
	}

	return true;
}

void USerial::Flush()
{
	if (!m_hIDComDev) return;

	TArray<uint8> Data;

	do {
		Data = ReadBytes(8192);
	} while (Data.Num() > 0);
}

FString USerial::LineEndToStr(ELineEnd LineEnd)
{
	switch (LineEnd)
	{
	case ELineEnd::rn:
		return TEXT("\r\n");
	case ELineEnd::n:
		return TEXT("\n");
	case ELineEnd::r:
		return TEXT("\r");
	case ELineEnd::nr:
		return TEXT("\n\r");
	default:
		return TEXT("null");
	}
}