// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Windows/AllowWindowsPlatformTypes.h"
#include "windows.h"
#include "Windows/HideWindowsPlatformTypes.h"
#include "CoreTypes.h"
#include "xrmouse.generated.h"


UCLASS()
class ARDUINOKIT_API Axrmouse : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Axrmouse();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	


	
	
	
};
